FROM ubuntu:20.04
RUN apt update -y
WORKDIR /home/unscanned-files
RUN apt install clamav clamav-daemon -y
RUN clamscan --version
RUN freshclam